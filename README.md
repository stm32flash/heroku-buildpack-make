Heroku buildpack: C and friends
===================

This is a [Heroku buildpack](http://devcenter.heroku.com/articles/buildpacks) for apps built using [Make](http://www.gnu.org/software/make/).

Usage
-----

Example usage:

    $ ls
    configure  Makefile  myapp.c

    $ heroku create --stack cedar --buildpack http://github.com/heroku/heroku-buildpack-c.git

    $ git push heroku master
    ...
    -----> Heroku receiving push
    -----> Fetching custom buildpack
    -----> C app detected
    -----> Configuring
           Looking for somelibrary… ok
    -----> Compiling with Make
           gcc -o myapp myapp.c

The buildpack will detect your app if it has a Makefile in the root.  It will also run an `autogen.sh` or `configure` script if it exists in the root of the repository. It will finally run `make` to compile the app.

Hacking
-------

To change this buildpack, fork it on your favorite git hosting service.  Push up changes to your fork, then create a test app with `--buildpack <your-git-url>` and push to it.

Commit and push the changes to your buildpack to your fork, then push your sample app to Heroku to test.

